import random, string, sys
methods = ['string', 'number', 'punc']
password = []
password_length = int(sys.argv[1])
for i in range(0,password_length):
	ran = random.randint(0,20000)
	if (methods[ran % 3] == "string"):
		password.append(random.choice(string.ascii_letters))
	elif (methods[ran % 3] == "punc"):
		password.append(random.choice(string.punctuation))
	elif (methods[ran % 3] == "number"):
		password.append(random.randint(0,9))

pw = "".join(str(p) for p in password)
print(pw)
