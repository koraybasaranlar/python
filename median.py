def median(numbers):
  if len(numbers) % 2 == 0:
    nmbrs = sorted(numbers)
    return float(nmbrs[((len(nmbrs)/2)-1)] + nmbrs[len(nmbrs)/2])/2
  else :
    n = sorted(numbers)
    if len(n) == 1:
      return n[0]
    else :
      return n[(len(n)-1)/2]