def divide_by_5(number):
    numbers = [] 
    for i in range(1, number+1):
        if i % 5 == 0 :
            numbers.append(str(i))
    return ', '.join(numbers)
number = None
while number == None :
    number = input("Insert a 2-digit number please: ")
    if number.isnumeric() != True :
        print("Please type only numeric values!")
        print("Thanks :)")
        continue
    if len(number) == 3 :
        b = input("%d is a 3-digit number,do you still wish to continue?(Y/N)" %int(number))
        if b == "N":
            print("You typed N,end of discussion")
            number = None
            break
        elif b != 'Y':
            print("Only Y or N are accepted!")
            continue
    elif len(number) != 2 :
        print("%d is NOT a 2-digit number!" %int(number))
        number = None
        continue
if number != None :
    print("Numbers divisible by 5 are: {}".format(divide_by_5(int(number))))  