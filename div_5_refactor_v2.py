
def divide_by_5(number):
    numbers = [] 
    for i in range(1, number+1):
        if i % 5 == 0 :
            numbers.append(str(i))
    return ', '.join(numbers)


def present_yes_no(message) :
    while True: 
        b = input("%s: do you still wish to continue?(Y/N)" % message)
        if b == "Y":
            return True
        if b == "N":
            print("You typed N, end of discussion")
            return False
        print("Only Y or N are accepted!")


def is_number_allowed(number):
    if len(number) == 2 :
        return True
    if len(number) == 3 :
        return True
    return False


number = None
while number == None :

    number = input("Insert a 2-digit number please: ")
    if number.isnumeric() != True :
        print("Please type only numeric values!")
        print("Thanks :)")
        number = None
        continue

    if is_number_allowed(number) == False:
        print("%d is NOT a 2 or 3 digit number!" %int(number))
        number = None
        continue

    if len(number) == 2 :
        break

    if len(number) == 3 :
        message = "%s is a 3-digit number" % number
        if present_yes_no(message):
            continue

    number = None

print("Numbers divisible by 5 are: {}".format(divide_by_5(int(number))))  
