import random
import string

password = []
for i in range(0,8):
	if (i % 3 == 0):
		password.append(random.choice(string.ascii_letters))
	elif (i % 3 == 2):
		password.append(random.choice(string.punctuation))
	elif (i % 3 == 1):
		password.append(random.randint(0,9))

pw = "".join(str(p) for p in password)
print(pw)