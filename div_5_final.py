def numberization(number):
    numbers = ""
    nmbrs = []
    for i in range(1, number+1):
        if i % 5 == 0 :
            #numbers = numbers + str(i) + ", "
            nmbrs.append(str(i))
        else :
            #print("%d is NOT divisible by 5" %i)
            pass
    #return numbers.strip(", ")
    return ', '.join(nmbrs)

pointer = False
while True :
    if pointer == False :
        number = input("Insert a 2-digit number please: ")
    if number.isnumeric() != True :
        print("Please type only numeric values!")
        print("Thanks :)")
        continue
    if len(number) == 3 :
        b = input("%d is a 3-digit number,do you still wish to continue?(Y/N)" %int(number))
        if b == "Y":
            pass
        elif b == "N" :
            print("You typed N,end of discussion")
            break
        else :
            print("Only Y or N are accepted!")
            pointer = True
            continue
    elif len(number) != 2 :
        print("%d is NOT a 2-digit number!" %int(number))
        continue
    print("Numbers divisible by 5 are: {}".format(numberization(int(number))))    
    
    break
